local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Builder = require("components/builder_replica")

--Zarklord: blindly replace this function, since idk what else to do...
function Builder:CanBuildAtPoint(pt, recipe, rot)
    return TheWorld.Map:CanDeployRecipeAtPoint(pt, recipe, rot, self.inst)
end

function Builder:SetIsJellyBrainHat(isjellybrainhat)
    if self.classified ~= nil then
        self.classified.isjellybrainhat:set(isjellybrainhat)
    end
end

local _KnowsRecipe = Builder.KnowsRecipe
function Builder:KnowsRecipe(recipe, ...)
    if self.inst.components.builder ~= nil then
        return _KnowsRecipe(self, recipe, ...)
    elseif self.classified ~= nil then
        local knows = _KnowsRecipe(self, recipe, ...)
        if not knows and self.classified.isjellybrainhat:value() then
            if type(recipe) == "string" then
                recipe = GetValidRecipe(recipe)
            end

            if recipe ~= nil then
                local valid_tech = true
                for techname, level in pairs(recipe.level) do
                    if level ~= 0 and (TECH.LOST[techname] or 0) == 0 then
                        valid_tech = false
                        break
                    end
                end
                for i, v in ipairs(recipe.tech_ingredients) do
                    if not self:HasTechIngredient(v) then
                        valid_tech = false
                        break
                    end
                end
                if string.find(recipe.name, "hermitshop") then
                    valid_tech = false
                end
                knows = valid_tech and (recipe.builder_tag == nil or self.inst:HasTag(recipe.builder_tag))
            end
        end
        return knows
    end
end
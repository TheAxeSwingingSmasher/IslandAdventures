local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

New_StartDanger = function (player, ...) end  -- for kraken music

IAENV.AddComponentPostInit("dynamicmusic", function(cmp)
    if IA_CONFIG.dynamicmusic == false then return end
	local OnPlayerActivated, OnPlayerDeactivated
    local StartPlayerListeners, StopSoundEmitter, StopPlayerListeners, StartSoundEmitter
    for i, v in ipairs(cmp.inst.event_listening["playeractivated"][TheWorld]) do
    	if UpvalueHacker.GetUpvalue(v, "StartPlayerListeners") then
    		StartPlayerListeners = UpvalueHacker.GetUpvalue(v, "StartPlayerListeners")
			OnPlayerActivated = v
    	end

		if UpvalueHacker.GetUpvalue(v, "StartSoundEmitter") then
			StartSoundEmitter = UpvalueHacker.GetUpvalue(v, "StartSoundEmitter")
		end

		if UpvalueHacker.GetUpvalue(v, "StopSoundEmitter") then
			StopSoundEmitter = UpvalueHacker.GetUpvalue(v, "StopSoundEmitter")
		end

		if UpvalueHacker.GetUpvalue(v, "StopPlayerListeners") then
			StopPlayerListeners = UpvalueHacker.GetUpvalue(v, "StopPlayerListeners")
		end
    end

	for i, v in ipairs(cmp.inst.event_listening["playerdeactivated"][TheWorld]) do
		if UpvalueHacker.GetUpvalue(v, "StopPlayerListeners") then
			OnPlayerDeactivated = v
			break
		end
	end

    if not (StartPlayerListeners and StartSoundEmitter and StopSoundEmitter and StopPlayerListeners and OnPlayerActivated and OnPlayerDeactivated) then return end

    local StartBusy = UpvalueHacker.GetUpvalue(StartPlayerListeners, "StartBusy")
	local StopBusy = UpvalueHacker.GetUpvalue(StopSoundEmitter, "StopBusy")
	local StopDanger = UpvalueHacker.GetUpvalue(StopSoundEmitter, "StopDanger")

    if not StartBusy or not StopBusy or not StopDanger then return end

    local _playsIAmusic = false

    local soundAlias = {
    	--busy
    	["dontstarve/music/music_work"] = "ia/music/music_work_season_1",
    	["dontstarve/music/music_work_winter"] = "ia/music/music_work_season_2",
    	["dontstarve_DLC001/music/music_work_spring"] = "ia/music/music_work_season_3",
    	["dontstarve_DLC001/music/music_work_summer"] = "ia/music/music_work_season_4",
    	--combat
    	["dontstarve/music/music_danger"] = "ia/music/music_danger_season_1",
    	["dontstarve/music/music_danger_winter"] = "ia/music/music_danger_season_2",
    	["dontstarve_DLC001/music/music_danger_spring"] = "ia/music/music_danger_season_3",
    	["dontstarve_DLC001/music/music_danger_summer"] = "ia/music/music_danger_season_4",
    	--epic
    	["dontstarve/music/music_epicfight"] = "ia/music/music_epicfight_season_1",
    	["dontstarve/music/music_epicfight_winter"] = "ia/music/music_epicfight_season_2",
    	["dontstarve_DLC001/music/music_epicfight_spring"] = "ia/music/music_epicfight_season_3",
    	["dontstarve_DLC001/music/music_epicfight_summer"] = "ia/music/music_epicfight_season_4",
    	--stinger
    	["dontstarve/music/music_dawn_stinger"] = "ia/music/music_dawn_stinger",
    	["dontstarve/music/music_dusk_stinger"] = "ia/music/music_dusk_stinger",
    }

    cmp.iamusictask = cmp.inst:DoPeriodicTask(1, function()
    	if not (ThePlayer and ThePlayer:IsValid()) then return end
    	if IsInIAClimate(ThePlayer) then
    		if not _playsIAmusic then
    			-- print("CHANGETO IA MUSIC")
    			for k, v in pairs(soundAlias) do
    				SetSoundAlias(k,v)
    			end

    			_playsIAmusic = true
    			UpvalueHacker.SetUpvalue(StartBusy, true, "_isbusydirty")
    		end
    	elseif _playsIAmusic then
    		-- print("CHANGETO ANR MUSIC")
    		for k, v in pairs(soundAlias) do
    			SetSoundAlias(k,nil)
    		end

    		_playsIAmusic = false
    		UpvalueHacker.SetUpvalue(StartBusy, true, "_isbusydirty")
    	end
    end)


-------------------------------Hook OnEnableDynamicMusic----------------------------

	local _isenabled = nil
	local _soundemitter = nil

	local OnEnableDynamicMusic = nil
	local New_OnEnableDynamicMusic = function(inst, enable, ...) end
	if cmp.inst.event_listening["enabledynamicmusic"] then
		for i, v in ipairs(cmp.inst.event_listening["enabledynamicmusic"][TheWorld]) do
			if UpvalueHacker.GetUpvalue(v, "StopDanger") then
				OnEnableDynamicMusic = v
				break
			end
		end

		if OnEnableDynamicMusic then
			New_OnEnableDynamicMusic = function(inst, enable, ...)

				_isenabled = UpvalueHacker.GetUpvalue(OnEnableDynamicMusic, "_isenabled")
				_soundemitter = UpvalueHacker.GetUpvalue(OnEnableDynamicMusic, "_soundemitter")

				if _isenabled ~= enable then
					if not enable and _soundemitter ~= nil then
						cmp:StopPlayingBoating()
						cmp:StopPlayingSurfing()
						cmp:StopPlayingErupt()
					end
					_isenabled = enable
				end
				print(enable)
				OnEnableDynamicMusic(inst, enable, ...)
			end

			cmp.inst:RemoveEventCallback("enabledynamicmusic", OnEnableDynamicMusic)
			cmp.inst:ListenForEvent("enabledynamicmusic", New_OnEnableDynamicMusic)
		end
	end

------------------------------------------------------------------------------------


---------------------------------Hook StopSoundEmitter------------------------------

	local extendtime = function() return UpvalueHacker.GetUpvalue(StartSoundEmitter, "_extendtime") end
	local function New_StopSoundEmitter(...)
		if _soundemitter ~= nil then
			cmp:StopPlayingBoating()
			cmp:StartPlayingSurfing()
			cmp:StopPlayingErupt()
			_soundemitter = nil
		end

		StopSoundEmitter(...)
	end

	UpvalueHacker.SetUpvalue(OnPlayerActivated, New_StopSoundEmitter, "StopSoundEmitter")
	UpvalueHacker.SetUpvalue(OnPlayerDeactivated, New_StopSoundEmitter, "StopSoundEmitter")

------------------------------------------------------------------------------------


---------------------------------Hook StartSoundEmitter-----------------------------

	local function New_StartSoundEmitter()
		StartSoundEmitter()
		_soundemitter = UpvalueHacker.GetUpvalue(StartSoundEmitter, "_soundemitter")
	end

	UpvalueHacker.SetUpvalue(OnPlayerActivated, New_StartSoundEmitter, "StartSoundEmitter")

------------------------------------------------------------------------------------


-----------------------------------Hook StartDanger---------------------------------

	local OnAttacked = UpvalueHacker.GetUpvalue(StartPlayerListeners, "OnAttacked")
	local StartDanger = UpvalueHacker.GetUpvalue(OnAttacked, "StartDanger")
	local dangertask = function () return UpvalueHacker.GetUpvalue(StartDanger, "_dangertask") end

	function New_StartDanger(player, ...)
		local _dangertask = dangertask()
		if _dangertask == nil and _isenabled then
			cmp:StopPlayingBoating()
			cmp:StopPlayingSurfing()
		end

		if StartDanger then
			StartDanger(player, ...)
		end
	end

	UpvalueHacker.SetUpvalue(OnAttacked, New_StartDanger, "StartDanger")

	local CheckAction = UpvalueHacker.GetUpvalue(StartPlayerListeners, "CheckAction")
	UpvalueHacker.SetUpvalue(CheckAction, New_StartDanger, "StartDanger")

------------------------------------------------------------------------------------


-------------------------------Hook StartTriggeredDanger----------------------------

	local StartTriggeredDanger = UpvalueHacker.GetUpvalue(StartPlayerListeners, "StartTriggeredDanger")
	local triggeredlevel = function() return UpvalueHacker.GetUpvalue(StartTriggeredDanger, "_triggeredlevel") end

	local function New_StartTriggeredDanger (player, data, ...)
		local level = math.max(1, math.floor(data ~= nil and data.level or 1))
		local _triggeredlevel = triggeredlevel()
		if _triggeredlevel ~= level and _isenabled then
			cmp:StopPlayingBoating()
			cmp:StopPlayingSurfing()
		end

		StartTriggeredDanger(player, data, ...)
	end

	UpvalueHacker.SetUpvalue(StartPlayerListeners, New_StartTriggeredDanger, "StartTriggeredDanger")
	UpvalueHacker.SetUpvalue(StopPlayerListeners, New_StartTriggeredDanger, "StartTriggeredDanger")

------------------------------------------------------------------------------------

------------------------------------Hook StartBusy----------------------------------
	local busytask = function () return UpvalueHacker.GetUpvalue(StartBusy, "_busytask") end

	local function New_StartBusy(player, ...)
		if not cmp.is_boating then
			StartBusy(player, ...)
		end
	end

	UpvalueHacker.SetUpvalue(CheckAction, New_StartBusy, "StartBusy")
	UpvalueHacker.SetUpvalue(StartPlayerListeners, New_StartBusy, "StartBusy")
------------------------------------------------------------------------------------


------------------------------new listen player functon ----------------------------

	local last_pos
	local sailing = false
	local extend_stoptime = math.huge

	local function OnPlayerMount(player)
		local sailor = player.replica.sailor
		if sailor and not sailor:IsSailing() then  --some mod player can fly
			last_pos = nil
			sailing = false
			extend_stoptime = math.huge
			player:PushEvent("dismountboat")
			return
		end

		local pos = player:GetPosition()
		if not last_pos then
			last_pos = pos
		end

		local time = GetTime()
		if  time >= extend_stoptime then
			sailing = false
			extend_stoptime = math.huge
			player:PushEvent("dismountboat")
		end

		if last_pos.x == pos.x and last_pos.y == pos.y and last_pos.z == pos.z then
			if sailing then
				extend_stoptime = time + 15
			end
		else
			extend_stoptime = math.huge
			sailing = true
			player:PushEvent("mountboat", {boat = sailor:GetBoat()})
			last_pos = pos
		end
	end

	local function OnPlayerMountBoat(player, data)
		if data and data.boat and data.boat.prefab == "player_boat_surfboard" then
			cmp:OnStartSurfing(player)
		else
			cmp:OnStartBoating(player)
		end
	end

	local function OnDisMountBoat(player, data)
		cmp:StopPlayingBoating()
		cmp:StopPlayingSurfing()
	end

	local function OnVolcanoEruptionBegin(player)
		cmp:OnStartErupt(player)
	end

	local function OnVolcanoEruptionEnd(player)
		cmp:StopPlayingErupt()
	end

------------------------------------------------------------------------------------


------------------------------hook StartPlayerListeners-----------------------------

	local function New_StartPlayerListeners(player, ...)
		StartPlayerListeners(player, ...)

		player:DoPeriodicTask(1, OnPlayerMount)
		cmp.inst:ListenForEvent("mountboat", OnPlayerMountBoat, player)
		cmp.inst:ListenForEvent("dismountboat", OnDisMountBoat, player)

		cmp.inst:ListenForEvent("OnVolcanoEruptionBegin", OnVolcanoEruptionBegin, player)
		cmp.inst:ListenForEvent("OnVolcanoEruptionEnd", OnVolcanoEruptionEnd, player)
	end

	UpvalueHacker.SetUpvalue(OnPlayerActivated, New_StartPlayerListeners, "StartPlayerListeners")

------------------------------------------------------------------------------------


------------------------------hook StopPlayerListeners------------------------------

	local function New_StopPlayerListeners(player, ...)
		StopPlayerListeners(player, ...)

		cmp.inst:RemoveEventCallback("mountboat", OnPlayerMountBoat, player)
		cmp.inst:RemoveEventCallback("dismountboat", OnDisMountBoat, player)

		cmp.inst:RemoveEventCallback("OnVolcanoEruptionBegin", OnVolcanoEruptionBegin, player)
		cmp.inst:RemoveEventCallback("OnVolcanoEruptionEnd", OnVolcanoEruptionEnd, player)
	end

	UpvalueHacker.SetUpvalue(OnPlayerActivated, New_StopPlayerListeners, "StopPlayerListeners")
	UpvalueHacker.SetUpvalue(OnPlayerDeactivated, New_StopPlayerListeners, "StopPlayerListeners")

------------------------------------------------------------------------------------


----------------------------------add boating music---------------------------------

	function cmp:StartPlayingBoating(player)
		if self.inst:HasTag("island") and player and IsInIAClimate(player) and _soundemitter then
			if TheWorld.state.isday then
				_soundemitter:PlaySound("ia/music/music_sailing_day", "boating_day")
				_soundemitter:SetParameter("boating_day", "intensity", 0)
			else
				_soundemitter:PlaySound("ia/music/music_sailing_night", "boating_night")
				_soundemitter:SetParameter("boating_night", "intensity", 0)
			end
		end
	end

	function cmp:StopPlayingBoating()
		if _soundemitter then
			_soundemitter:SetParameter("boating_day", "intensity", 0)
			_soundemitter:SetParameter("boating_night", "intensity", 0)
			self.is_boating = false
		end
	end

	function cmp:OnStartBoating(player)
		if not _isenabled or not player or not IsInIAClimate(player) or not _soundemitter then
			return
		end

		self:StartPlayingBoating(player)
		self:StopPlayingSurfing()

		if not _soundemitter:PlayingSound("dawn") then

			if not self.is_boating then
				self.is_boating = true
				local _dangertask = dangertask()
				local _extendtime = extendtime() or 0
				if _extendtime <= GetTime() and not _dangertask and not _soundemitter:PlayingSound("erupt") then
					StopBusy(self.inst)

					if cmp.inst.state.isday then
						_soundemitter:SetParameter("boating_day", "intensity", 1)
					else
						_soundemitter:SetParameter("boating_night", "intensity", 1)
					end

					self.inst:WatchWorldState("phase", function(inst, phase)
						if phase == "day" or phase == "dusk" then
							self:StopPlayingBoating()
							self:StopPlayingSurfing()
						end
					end)

				end
			end
		end
	end

------------------------------------------------------------------------------------


----------------------------------add surfing music---------------------------------

	function cmp:StartPlayingSurfing(player)
		if self.inst:HasTag("island") and player and IsInIAClimate(player) and _soundemitter then
			_soundemitter:KillSound("surfing")
			if TheWorld.state.isday then
				_soundemitter:PlaySound("ia/music/music_surfing_day", "surfing_day")
			else
				_soundemitter:PlaySound("ia/music/music_surfing_night", "surfing_night")
			end
			_soundemitter:SetParameter("surfing_day", "intensity", 0)
			_soundemitter:SetParameter("surfing_night", "intensity", 0)
		end
	end

	function cmp:StopPlayingSurfing()
		if _soundemitter then
			_soundemitter:SetParameter("surfing_day", "intensity", 0)
			_soundemitter:SetParameter("surfing_night", "intensity", 0)
			self.is_boating = false
		end
	end

	function cmp:OnStartSurfing(player)
		if not _isenabled or not player or not IsInIAClimate(player) or not _soundemitter then
			return
		end

		self:StartPlayingSurfing(player)
		self:StopPlayingSurfing()

		if not _soundemitter:PlayingSound("dawn") then

			if not self.is_boating then
				self.is_boating = true
				local _dangertask = dangertask()
				local _extendtime = extendtime() or 0
				if _extendtime <= GetTime() and not _dangertask and not _soundemitter:PlayingSound("erupt") then
					StopBusy(self.inst)

					if cmp.inst.state.isday then
						_soundemitter:SetParameter("surfing_day", "intensity", 1)
					else
						_soundemitter:SetParameter("surfing_night", "intensity", 1)
					end

					self.inst:WatchWorldState("phase", function(inst, phase)
						if phase == "day" or phase == "dusk" then
							self:StopPlayingBoating()
							self:StopPlayingSurfing()
						end
					end)

				end
			end
		end
	end

------------------------------------------------------------------------------------


--------------------------------add volcano music-----------------------------------

	function cmp:OnStartErupt(player)
		if not _isenabled or not player or not IsInIAClimate(player) or not _soundemitter then
			return
		end

		if not _soundemitter:PlayingSound("erupt") then

			New_StopPlayerListeners(player)
            StopBusy()
			StopDanger()
			cmp:StopPlayingBoating()
			cmp:StopPlayingSurfing()

			_soundemitter:PlaySound("ia/music/music_volcano_active", "erupt")
			_soundemitter:KillSound("dawn")

			self.inst:DoTaskInTime(12, function()  --in music_volcano_active end
				New_StartPlayerListeners(player)
			end)
		end

		self.isErupting = true
	end

	function cmp:StopPlayingErupt()
		if _soundemitter then
			_soundemitter:KillSound("erupt")
			self.isErupting = false
		end
	end

	function cmp:OnPlayerArriveVolcano(player)
		if player and player == ThePlayer and TheWorld:HasTag("volcano") and _soundemitter then
			if self.isErupting or player.player_classified.smokerate:value() > 0 then
				_soundemitter:PlaySound("ia/music/music_volcano_active")
			else
				_soundemitter:PlaySound("ia/music/music_volcano_dormant")
			end
		end
	end

	cmp.inst:ListenForEvent("playerentered", cmp.OnPlayerArriveVolcano)
end)

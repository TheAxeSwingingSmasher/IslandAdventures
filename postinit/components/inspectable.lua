local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Inspectable = require("components/inspectable")

local _GetStatus = Inspectable.GetStatus
function Inspectable:GetStatus(viewer, ...)
	return _GetStatus(self, viewer, ...) or self.inst ~= viewer and self.inst:HasTag("flooded") and "FLOODED" or nil
end

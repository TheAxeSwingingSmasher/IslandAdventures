local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Inventory = require("components/inventory")

local _GetEquippedItem = Inventory.GetEquippedItem
function Inventory:GetEquippedItem(eslot)
    if eslot == nil then
        return false
    else
        return _GetEquippedItem(self, eslot)
    end
end

local _Equip = Inventory.Equip
function Inventory:Equip(item, ...)
	if item == nil or item.components.equippable == nil or item.components.equippable.equipslot == nil then
        return false
    else
        return _Equip(self, item, ...)
    end
end

function Inventory:HasEquip(prefab)
    for k, v in pairs(self.equipslots) do
        if v.prefab == prefab then
            return true
        end
    end
end

function Inventory:GetWindproofness(slot)
    local windproofness = 0
    if slot then
        local item = self:GetItemSlot(slot)
        if item and item.components.windproofer then
            windproofness = windproofness + item.components.windproofer.GetEffectiveness()
        end
    else
        for k,v in pairs(self.equipslots) do
            if v and v.components.windproofer then
                windproofness = windproofness + v.components.windproofer:GetEffectiveness()  
            end
        end
    end
    return windproofness
end

function Inventory:DropItemBySlot(slot)
    local item = self:RemoveItemBySlot(slot)
    if item ~= nil then
        item.Transform:SetPosition(self.inst.Transform:GetWorldPosition())
        if item.components.inventoryitem ~= nil then
            item.components.inventoryitem:OnDropped(true)
        end
        item.prevcontainer = nil
        item.prevslot = nil
        self.inst:PushEvent("dropitem", { item = item })
    end
end

local _DropItem = Inventory.DropItem
function Inventory:DropItem(item, wholestack, randomdir, pos, ...)
    if item == nil or item.components.inventoryitem == nil then
        return
    end
    if item.components.inventoryitem.tossdir ~= nil then
        --print("ERROR: Boat toss still set, please report this!!")
        item.components.inventoryitem.tossdir = nil --this still happens with full inventorys...
    end
    if self.inst.components.sailor and self.inst.components.sailor:IsSailing() then

        local dropped = item.components.inventoryitem:RemoveFromOwner(wholestack) or item

        if dropped ~= nil then
            if pos ~= nil then
                dropped.Transform:SetPosition(pos:Get())
            else
                dropped.Transform:SetPosition(self.inst.Transform:GetWorldPosition())
            end

            if dropped.components.inventoryitem ~= nil then
                local rot = self.inst.Transform:GetRotation() * DEGREES
                -- print("my rot", rot)
                local tossdir = Vector3(0,0,0)
                tossdir.x = math.cos(rot)
                tossdir.z = -math.sin(rot)
                dropped.components.inventoryitem.tossdir = tossdir --save the direction of the owner for tossing, also used to stop player tossed items from causing sharx/croc attacks
                dropped.components.inventoryitem:OnDropped(0,0)
            end

            dropped.prevcontainer = nil
            dropped.prevslot = nil

            self.inst:PushEvent("dropitem", { item = dropped })
        end

        return dropped
    else
        return _DropItem(self, item, wholestack, randomdir, pos, ...)
    end
end

function Inventory:IsWindproof()
    return self:GetWindproofness() >= 1
end


----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

local function InvSpaceChanged(inst)
    inst:PushEvent("invspacechange", {percent = inst.components.inventory:NumItems() / inst.components.inventory.maxslots})
end

IAENV.AddComponentPostInit("inventory", function(cmp)
    cmp.inst:ListenForEvent("itemget", InvSpaceChanged)
    cmp.inst:ListenForEvent("itemlose", InvSpaceChanged)
    cmp.inst:ListenForEvent("dropitem", InvSpaceChanged)
end)

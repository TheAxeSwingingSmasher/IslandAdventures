local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local InventoryItem = require("components/inventoryitem")

--copy with our stuff
local function sink_item(item)
    if not item:IsValid() or item:CanOnWater() then
        return
    end

    local px, py, pz = 0, 0, 0
    if item.Transform ~= nil then
        px, py, pz = item.Transform:GetWorldPosition()
    end

    local tile = TheWorld.Map:GetTileAtPoint(px, py, pz)

    if IsOceanTile(tile) or (not TheWorld:HasTag("volcano") and tile == GROUND.IMPASSABLE) then
        return SinkEntity(item)
    else
        local fx = SpawnPrefab((TheWorld:HasTag("volcano") and ((ChangeToLava(px, py, pz) and "splash_lava_drop") or "splash_clouds_drop")) or "splash_water_sink")
        fx.Transform:SetPosition(px, py, pz)
        --sink sound is done by the fx
    end


    -- If the item is irreplaceable, respawn it at the player
    if item:HasTag("irreplaceable") then
        if TheWorld.components.playerspawner ~= nil then
            item.Transform:SetPosition(TheWorld.components.playerspawner:GetAnySpawnPoint())
        else
            -- Our reasonable cases are out... so let's loop to find the portal and respawn there.
            for k, v in pairs(Ents) do
                if v:IsValid() and v:HasTag("multiplayer_portal") then
                    item.Transform:SetPosition(v.Transform:GetWorldPosition())
                end
            end
        end
    else
        --if (item:HasTag("irreplaceable") or (tile ~= GROUND.OCEAN_DEEP and tile ~= GROUND.IMPASSABLE and tile ~= GROUND.VOLCANO_LAVA and tile ~= GROUND.VOLCANO_CLOUD))
        if (item:HasTag("irreplaceable") or (tile ~= GROUND.OCEAN_DEEP and tile ~= GROUND.IMPASSABLE))
        and item.components.inventoryitem
        and item.components.inventoryitem.cangoincontainer
        and item.persists
        and not item.nosunkenprefab then
            SpawnPrefab("sunkenprefab"):Initialize(item)
        end
        item:Remove()
	end
end

local _OnPickup = InventoryItem.OnPickup
function InventoryItem:OnPickup(pickupguy, src_pos, ...)
    self.tossdir = nil
    return _OnPickup(self, pickupguy, src_pos, ...)
end

local _OnPutInInventory = InventoryItem.OnPutInInventory
function InventoryItem:OnPutInInventory(owner, ...)
    self.tossdir = nil
    return _OnPutInInventory(self, owner, ...)
end

--TODO add monkeyball onlanded bounce sounds
--local _SetLanded = InventoryItem.SetLanded
--function InventoryItem:SetLanded(is_landed, should_poll_for_landing, ...)
--    _SetLanded(self, is_landed, should_poll_for_landing, ...)
    --if self.bouncesound and self.inst.Physics and self.inst.SoundEmitter and not is_landed and should_poll_for_landing then
    --    self.bouncetime = GetTime()
    --end
--end

local _DoDropPhysics = InventoryItem.DoDropPhysics
function InventoryItem:DoDropPhysics(x, y, z, randomdir, speedmult, ...)
    if self.tossdir then
        local tossdir = self.tossdir

        self:SetLanded(false, true)

        if self.inst.Physics ~= nil then
            local heavy = self.inst:HasTag("heavy")
            if not self.nobounce then
                y = y + (heavy and .5 or 1)
            end
            self.inst.Physics:Teleport(x, y, z)

            local vel = Vector3(0, 5, 0)
            if tossdir then
                vel.x = tossdir.x * 4--speed
                vel.z = tossdir.z * 4--speed
                self.inst.Physics:Teleport(x + tossdir.x ,y,z + tossdir.z) --move the position a bit so it doesn't clip through the player
            else
                vel.x = 0
                vel.y = (self.nobounce and 0) or (heavy and 2.5) or 5
                vel.z = 0
            end
            self.inst.Physics:SetVel(vel.x, vel.y, vel.z)
        else
            self.inst.Transform:SetPosition(x, y, z)
        end
    else
        _DoDropPhysics(self, x, y, z, randomdir, speedmult, ...)
    end
end

--copy of SetLanded but forces it to send the event regardless of if its already landed along with some other improvements, much better than refreshing it each time -Half
function InventoryItem:ForceLanded(is_landed, should_poll_for_landing)
    if is_landed ~= nil then --if nothing has been set dont change anything
        if not is_landed then

            -- If we're going from landed to not landed
            if self.pushlandedevents then
                self.inst:PushEvent("on_no_longer_landed")
            end
        else

            -- If we're going from not landed to landed
            if self.pushlandedevents then
                self.inst:PushEvent("on_landed")
                self:TryToSink()
            end
        end
    end
    if should_poll_for_landing ~= nil then --if nothing has been set dont change anything
        if should_poll_for_landing then
            self.inst:StartUpdatingComponent(self)
        else
            self.inst:StopUpdatingComponent(self)
        end
    end

    self.is_landed = is_landed
end

--i hate overwriting the entire function but it must be done -Half
function InventoryItem:OnUpdate(dt)
    local x,y,z = self.inst.Transform:GetWorldPosition()

    if x and y and z then
        local vely = 0
        if self.inst.Physics then
            local vx, vy, vz = self.inst.Physics:GetVelocity()
            vely = vy or 0

            if (not vx) or (not vy) or (not vz) then
                self:SetLanded(true, false)
            elseif (vx == 0) and (vy == 0) and (vz == 0) then
                self:SetLanded(true, false)
            end
        end

        if y + vely * dt * 1.5 < 0.01 and vely <= 0 then
            --unlike the dst one the ds one doesnt stop updating at this point, instead only stoping the update when (vx == 0) and (vy == 0) and (vz == 0) or they dont exist
            self:ForceLanded(true)
        end
    else
        self:SetLanded(true, false)
    end
end

function InventoryItem:TryToSink()
    if self:ShouldSink() then
        self.inst:DoTaskInTime(0, sink_item)
    end
end

local _ShouldSink = InventoryItem.ShouldSink
function InventoryItem:ShouldSink(...)
	--as of right now, the effect only runs if not on a land tile, and IA water is land to the game...
	return self.inst:IsValid() and (_ShouldSink(self, ...) or (self.sinks and not self:IsHeld() and IsOnWater(self.inst)))
end

local _InheritMoisture = InventoryItem.InheritMoisture
function InventoryItem:InheritMoisture(moisture, iswet, ...)
    if moisture ~= 0 -- Not perfect, but reduce the chance of miss judge
        and moisture == TheWorld.state.wetness
        and iswet == TheWorld.state.iswet
        and IsInIAClimate(self.inst) then

        return _InheritMoisture(self, TheWorld.state.islandwetness, TheWorld.state.islandiswet, ...)
    end
    return _InheritMoisture(self, moisture, iswet, ...)
end

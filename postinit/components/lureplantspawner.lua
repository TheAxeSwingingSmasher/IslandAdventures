local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
---------------------------------



IAENV.AddComponentPostInit("lureplantspawner", function(cmp)
    local VALID_TILES
    for i, v in ipairs(cmp.inst.event_listening["ms_playerjoined"][TheWorld]) do
        if UpvalueHacker.GetUpvalue(v, "ScheduleTrailLog") then
            VALID_TILES =  UpvalueHacker.GetUpvalue(v, "ScheduleTrailLog", "LogPlayerLocation", "VALID_TILES")
            break
        end
    end
    if VALID_TILES then
        local IA_TILES = table.invert({GROUND.MEADOW,GROUND.JUNGLE,GROUND.TIDALMARSH,GROUND.BEACH})
        for i,v in pairs(IA_TILES) do
            VALID_TILES[i] = (#VALID_TILES+v+1)
        end
    end
end)

local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local PlayerController = require("components/playercontroller")

local _DoControllerUseItemOnSceneFromInvTile = PlayerController.DoControllerUseItemOnSceneFromInvTile
function PlayerController:DoControllerUseItemOnSceneFromInvTile(item, ...)
    local is_equipped = item ~= nil and item:IsValid() and item.replica.equippable and item.replica.equippable:IsEquipped()
    if is_equipped then
        self.inst.replica.inventory:ControllerUseItemOnSceneFromInvTile(item)
    else
        _DoControllerUseItemOnSceneFromInvTile(self, item, ...)
    end
end

local _GetGroundUseAction = PlayerController.GetGroundUseAction
function PlayerController:GetGroundUseAction(position, ...)
    if self.inst:HasTag("_sailor") and self.inst:HasTag("sailing") then
        if position ~= nil then
            local landingPos = Vector3(TheWorld.Map:GetTileCenterPoint(position.x, 0, position.z))
            if landingPos.x == position.x and landingPos.z == position.z then
                local l = nil
                local r = BufferedAction(self.inst, nil, ACTIONS.DISEMBARK, nil, landingPos)
                return l, r
            end
        else
            --Check if the player is close to land and facing towards it
            local angle = self.inst.Transform:GetRotation() * DEGREES
            local dir = Vector3(math.cos(angle), 0, -math.sin(angle))
            dir = dir:GetNormalized()

            local myPos = self.inst:GetPosition()
            local step = 0.4
            local numSteps = 8
            local landingPos = nil

            for i = 0, numSteps, 1 do
                local testPos = myPos + dir * step * i
                local testTile = TheWorld.Map:GetTileAtPoint(testPos.x , testPos.y, testPos.z)
                if not IsWater(testTile) then
                    landingPos = testPos
                    break
                end
            end
            if landingPos then
                landingPos.x, landingPos.y, landingPos.z = TheWorld.Map:GetTileCenterPoint(landingPos.x, 0, landingPos.z)
                local l = nil
                local r = BufferedAction(self.inst, nil, ACTIONS.DISEMBARK, nil, landingPos)
                return l, r
            end
        end
    end
    return _GetGroundUseAction(self, position, ...)
end

--[[local _OnUpdate = PlayerController.OnUpdate
function PlayerController:OnUpdate(dt)
    local OnUpdate_return = _OnUpdate(self, dt)
    if self.inst.replica.sailor and self.inst.replica.sailor:IsSailing() then
        --do automagic control repeats
        local isbusy = self:IsBusy()
        if isbusy and self.inst.sg ~= nil and self.inst:HasTag("jumping") then
            isbusy = isbusy and self.inst.sg:HasStateTag("jumping")
        end
        local isenabled, ishudblocking = self:IsEnabled()
        if not OnUpdate_return and isenabled and not (self.ismastersim and self.handler == nil and not self.inst.sg.mem.localchainattack) and (self.ismastersim or self.handler ~= nil)
            and not (self.directwalking or isbusy)
            and not (self.locomotor ~= nil and self.locomotor.bufferedaction ~= nil and self.locomotor.bufferedaction.action == ACTIONS.CASTAOE) then
            local attack_control = false
            if self.inst.sg ~= nil then
                attack_control = not self.inst.sg:HasStateTag("attack")
            else
                attack_control = not self.inst:HasTag("attack")
            end
            if attack_control and (self.inst.replica.combat == nil or not self.inst.replica.combat:InCooldown()) then
                attack_control = (self.handler == nil or not IsPaused())
                    and ((self:IsControlPressed(CONTROL_ATTACK) and CONTROL_ATTACK) or
                        (self:IsControlPressed(CONTROL_PRIMARY) and CONTROL_PRIMARY) or
                        (self:IsControlPressed(CONTROL_CONTROLLER_ATTACK) and not self:IsAOETargeting() and CONTROL_CONTROLLER_ATTACK))
                    or nil
                if attack_control ~= nil then
                    --Check for chain attacking first
                    local retarget = nil
                    local buffaction = self.inst:GetBufferedAction()
                    if buffaction and buffaction.action == ACTIONS.ATTACK then
                        retarget = buffaction.target
                    elseif self.inst.sg ~= nil then
                        retarget = self.inst.sg.statemem.attacktarget
                    elseif self.inst.replica.combat ~= nil then
                        retarget = self.inst.replica.combat:GetTarget()
                    end
                    if not (retarget and not IsEntityDead(retarget) and CanEntitySeeTarget(self.inst, retarget)) and self.handler ~= nil then
                    --elseif attack_control ~= CONTROL_PRIMARY and self.handler ~= nil then
                        --Check for starting a new attack, as long as the payer is not busy allow them to attack while on a boat, if the player is idle let the dst code handle it.
                        --this is to account for differences between dst and ds, allowing players to activate a held attack even while moving due to boat momentum
                        local isidle
                        if self.inst.sg ~= nil then
                            isidle = self.inst.sg:HasStateTag("idle") or (self.inst:HasTag("idle") and self.inst:HasTag("nopredict"))
                        else
                            isidle = self.inst:HasTag("idle")
                        end
                        if not isidle then
                            -- Check for primary control button held down in order to attack other nearby monsters
                            if attack_control == CONTROL_PRIMARY and self.actionholding then
                                if self.ismastersim then
                                    self.attack_buffer = CONTROL_ATTACK
                                else
                                    self:DoAttackButton()
                                end
                            elseif not TheInput:IsControlPressed(CONTROL_PRIMARY) then
                                self:OnControl(attack_control, true)
                            end
                        end
                    end
                end
            end
        end
    end
    return OnUpdate_return
end--]]

local _GetPickupAction, _fn_i, scope_fn = UpvalueHacker.GetUpvalue(PlayerController.GetActionButtonAction, "GetPickupAction")
local function GetPickupAction(self, target, tool, ...)
    if not target:HasTag("smolder") and tool ~= nil then
        for k, v in pairs(TOOLACTIONS) do
            if target:HasTag(k.."_workable") then
                if tool:HasTag(k.."_tool") then
                    return ACTIONS[k]
                end
                -- break  Remove this break myself bc zarklord not respond to me _(:3」∠)_
            end
        end
    end
    local rets = {_GetPickupAction(self, target, tool)}
    if rets[1] == nil then
        if target.replica.inventoryitem ~= nil and
        target.replica.inventoryitem:CanBePickedUp() and
        not (target:HasTag("heavy") or (not target:HasTag("ignoreburning") and target:HasTag("fire")) or target:HasTag("catchable")) and not target:HasTag("spider") then
            rets[1] = (self:HasItemSlots() or target.replica.equippable ~= nil) and ACTIONS.PICKUP or nil
        end
    end
    return unpack(rets)
end
debug.setupvalue(scope_fn, _fn_i, GetPickupAction)

if not TheNet:IsDedicated() then
    local _IsVisible = Entity.IsVisible
    local function IsVisibleNotLocalNOCLICKed(self)
        return not IsLocalNOCLICKed(self) and _IsVisible(self)
    end

    local _UpdateControllerTargets, _fn_i, scope_fn = UpvalueHacker.GetUpvalue(PlayerController.UpdateControllerTargets, "UpdateControllerInteractionTarget")
    debug.setupvalue(scope_fn, _fn_i, function(self, ...)
        Entity.IsVisible = IsVisibleNotLocalNOCLICKed
        _UpdateControllerTargets(self, ...)
        Entity.IsVisible = _IsVisible
    end)
    --YEAH YEAH, this isn't a player controller post init, but it fits the theme of preventing selection of a entity on the client. -Z
    local _GetEntitiesAtScreenPoint = Sim.GetEntitiesAtScreenPoint
    function Sim:GetEntitiesAtScreenPoint(...)
        local entlist = {}
        for i, ent in ipairs(_GetEntitiesAtScreenPoint(self, ...)) do
            if not IsLocalNOCLICKed(ent) then
                entlist[#entlist + 1] = ent
            end
        end
        return entlist
    end
end

local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local Old_OnSave
local function OnSave(inst,data)
    if inst.save_loot and inst.components.lootdropper and inst.components.lootdropper.loot then
        data.loot = inst.components.lootdropper.loot
    end
    data.ClimateLootSet = inst.ClimateLootSet
    if Old_OnSave then
        Old_OnSave(inst, data)
    end
end

local Old_OnLoad
local function OnLoad(inst,data)
    if data then
        if data.loot then
            inst.components.lootdropper:SetLoot(data.loot)
            inst.save_loot = true
        end
        inst.ClimateLootSet = data.ClimateLootSet
    end
    if Old_OnLoad then
        Old_OnLoad(inst,data)
    end
end


local function merm_postinit(inst)

    inst:AddTag("mermfighter")

    if TheWorld.ismastersim then
        if not inst.ClimateLootSet and IsInIAClimate(inst) then
            local remove = {}
            for index,loot in pairs(inst.components.lootdropper.loot) do
                if loot == "pondfish" then
                    inst.components.lootdropper.loot[index] = "pondfish_tropical"
                elseif loot == "kelp" then
                    inst.components.lootdropper.loot[index] = "seaweed"
                elseif loot == "froglegs" then --sw merms dont ever drop froglegs
                    table.insert(remove, index)
                end
            end
            for num,remove_index in pairs(remove) do
                table.remove(inst.components.lootdropper.loot, remove_index)
            end
            inst.save_loot = true
        end
        inst.ClimateLootSet = true
        --atm merms dont save or load anything -half
        if inst.OnSave  and not Old_OnSave then Old_OnSave = inst.OnSave end
        if inst.OnLoad and not Old_OnLoad then Old_OnLoad = inst.OnLoad end
        inst.OnSave = OnSave
        inst.OnLoad = OnLoad
    end

end

IAENV.AddPrefabPostInit("merm", merm_postinit)
IAENV.AddPrefabPostInit("mermguard", merm_postinit)

local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local function peruse_meteor(inst)
    inst.components.sanity:DoDelta(TUNING.SANITY_LARGE)
end

IAENV.AddPrefabPostInit("wurt", function(inst)

    if not TheNet:IsDedicated() then
        local _WARNING_MUST_TAGS
        for i, v in ipairs(inst.event_listening["playeractivated"][inst]) do
            if UpvalueHacker.GetUpvalue(v, "EnableTentacleWarning") then
                _WARNING_MUST_TAGS = UpvalueHacker.GetUpvalue(v, "EnableTentacleWarning", "UpdateTentacleWarnings", "WARNING_MUST_TAGS")
                break
            end
        end
        if _WARNING_MUST_TAGS ~= nil then
            for i,v in pairs(_WARNING_MUST_TAGS) do
                if v == "tentacle" then
                    _WARNING_MUST_TAGS[i] = "alert_wurt"
                end
            end
        end
    end

    if not TheWorld.ismastersim then
        return inst
    end 
    inst.components.itemaffinity:AddAffinity("packim_fishbone", nil, -TUNING.DAPPERNESS_MED, 2)
    inst.components.health.cantdrown_penalty = true
    inst.components.locomotor:SetFasterOnGroundTile(GROUND.TIDALMARSH, true)
    inst.components.foodaffinity:AddPrefabAffinity  ("seaweed", 1.33)
    inst.peruse_meteor = peruse_meteor

end)

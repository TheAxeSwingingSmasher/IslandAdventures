require "prefabutil"

local assets =
{
	Asset("ANIM", "anim/dragoon_den.zip"),
}

local prefabs =
{
	"dragoon",
}

SetSharedLootTable('dragoonden',
{
    {'dragoonheart', 1.000},
    {'rocks', 1},
	{'rocks', 1},
	{'rocks', 0.5},
	{'obsidian', 1},
	{'obsidian', 1},
})

local function ongohome(inst, child)
	inst.AnimState:PlayAnimation("hit")
	inst.AnimState:PushAnimation("idle")
end

local function StartSpawningFn(inst)
	inst.components.childspawner:StartSpawning()
end

local function StopSpawningFn(inst)
	inst.components.childspawner:StopSpawning()
end

local function onhammered(inst, worker)
	inst.components.lootdropper:DropLoot()
	SpawnPrefab("collapse_big").Transform:SetPosition(inst.Transform:GetWorldPosition())
	inst.SoundEmitter:PlaySound("dontstarve/common/destroy_stone")
	inst:Remove()
end

local function onhit(inst, worker)
	inst.AnimState:PlayAnimation("hit")
	inst.AnimState:PushAnimation("idle")
end

local function spawncheckday(inst)
    inst.inittask = nil
    inst:WatchWorldState("isdusk", StopSpawningFn)
	inst:WatchWorldState("isday", StartSpawningFn)
    if inst.components.childspawner and inst.components.childspawner.childreninside > 0 then
      if TheWorld.state.isday then 
		StartSpawningFn(inst)
      else
        StopSpawningFn(inst)
      end
    end
end

local function oninit(inst)
    inst.inittask = inst:DoTaskInTime(math.random(), spawncheckday)
end

local function onbuilt(inst)
	inst.AnimState:PlayAnimation("place")
	inst.AnimState:PushAnimation("idle")
	inst.SoundEmitter:PlaySound("ia/common/dragoon_den_place")
end

local function fn()
	local inst = CreateEntity()
	local trans = inst.entity:AddTransform()
	local anim = inst.entity:AddAnimState()
	inst.entity:AddSoundEmitter()
	inst.entity:AddNetwork()
	MakeObstaclePhysics(inst, 1.5)

	local minimap = inst.entity:AddMiniMapEntity() --temp
	minimap:SetIcon("dragoonden.tex")

	anim:SetBank("dragoon_den")
	anim:SetBuild("dragoon_den")
	anim:PlayAnimation("idle", true)

	inst:AddTag("structure")


    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("workable")
    inst.components.workable:SetWorkAction(nil)
	inst.components.workable:SetOnFinishCallback(onhammered)
	inst.components.workable:SetOnWorkCallback(onhit)

	inst:AddComponent( "childspawner" )
	inst.components.childspawner:SetRegenPeriod(120)
	inst.components.childspawner:SetSpawnPeriod(30)
	inst.components.childspawner:SetMaxChildren(math.random(3,4))
	inst.components.childspawner:StartRegen()
	inst.components.childspawner.childname = "dragoon"
	--inst.components.childspawner:StartSpawning()
	inst.components.childspawner.ongohome = ongohome

	inst:AddComponent("inspectable")

	inst:AddComponent("lootdropper")
	--inst.components.lootdropper:SetChanceLootTable('dragoonden')

	inst:ListenForEvent("onbuilt", onbuilt)
	inst.inittask = inst:DoTaskInTime(0, oninit)

	return inst
end

return Prefab("dragoonden", fn, assets, prefabs),
		MakePlacer("dragoonden_placer", "dragoon_den", "dragoon_den", "idle")
